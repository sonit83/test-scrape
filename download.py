from bs4 import BeautifulSoup
import requests

def get_file():
    url = 'https://europa.eu/european-union/about-eu/countries_en'
    response = requests.get(url, timeout=5)
    page_content = BeautifulSoup(response.content, "html.parser")
    title = page_content.title.string
    count_upper_letter = sum(1 for c in title if c.isupper())
    count_lower_letter = sum(1 for c in title if c.islower())
    count_space = title.count(' ')
    f = open('out.txt', 'w')
    f.write(title + '\r\n')
    f.write('count upper letters = ' + str(count_upper_letter) + '\r\n')
    f.write('count lower letters = ' + str(count_lower_letter) + '\r\n')
    f.write('count spaces = ' + str(count_space))
    f.close()


if __name__ == '__main__':
    get_file()

